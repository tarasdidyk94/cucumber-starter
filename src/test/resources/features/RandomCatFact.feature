Feature: Random cat facts
  As a user I would like to see random fact about cat

  Scenario: User sees a random fact about a cat
    When User calls random cat fact endpoint
    Then User should get result for cat facts
    And User should see information about cat facts
    And User should not see errors

  Scenario: Invalid cat category
    When User requests information about un existing about category
    Then User should sees a 404 status code
    And User should sees an "Not Found" error message
