Feature: All cat facts
  As a user I would like to see all facts about cats

  Scenario: User sees all facts about cats
    When User calls all cats facts endpoint
    Then User should get result for cat facts
    And User should see more than 10 cat facts
    And User should not see errors