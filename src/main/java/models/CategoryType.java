package models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum CategoryType {

    FACTS("facts"),
    ABOUT("about"),
    FACT("fact");

    private final String name;
}