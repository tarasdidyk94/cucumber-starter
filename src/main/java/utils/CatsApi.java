package utils;

import io.restassured.response.Response;
import models.CategoryType;
import net.serenitybdd.rest.SerenityRest;

public class CatsApi {
    private final String baseUrl = "https://catfact.ninja/";
    private final Response response;

    public CatsApi(CategoryType categoryType) {
        response = SerenityRest.given().get(baseUrl + categoryType.getName());
    }

    public int getResponseStatus() {
        return response.statusCode();
    }

    public String getResponseValue(String valueName) {
        return response.getBody().jsonPath().get(valueName);
    }

    public int getResponseIntValue(String valueName) {
        return response.getBody().jsonPath().getInt(valueName);
    }
}
