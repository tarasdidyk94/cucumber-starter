package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.CatsApi;

import static models.CategoryType.*;
import static org.assertj.core.api.Java6Assertions.assertThat;


public class CatFactsSteps {
    private CatsApi catsApi;

    @When("User calls random cat fact endpoint")
    public void userCallsCatFactEndpoint() {
        catsApi = new CatsApi(FACT);
    }

    @Then("User should get result for cat facts")
    public void userShouldGetResultForCatFacts() {
        assertThat(catsApi.getResponseStatus()).isEqualTo(200);
    }

    @Then("User should see information about cat facts")
    public void userShouldSeeInformationAboutCatFacts() {
        assertThat(catsApi.getResponseValue("fact")).isNotBlank();
    }

    @Then("User should not see errors")
    public void userShouldNotSeeError() {
        assertThat(catsApi.getResponseValue("error")).isNull();
    }

    @When("User requests information about un existing about category")
    public void userRequestsInformationAboutUnExistingCategory() {
        catsApi = new CatsApi(ABOUT);
    }

    @Then("User should sees a {int} status code")
    public void userShouldSeesStatusCode(int statusCode) {
        assertThat(catsApi.getResponseStatus()).isEqualTo(statusCode);
    }

    @And("User should sees an {string} error message")
    public void userShouldSeesErrorMessage(String errorMessage) {
        assertThat(catsApi.getResponseValue("message")).containsIgnoringCase(errorMessage);
    }

    @And("User should see more than {int} cat facts")
    public void userShouldSeeMoreThanCatFacts(int expectedGreaterThan) {
        assertThat(catsApi.getResponseIntValue("total")).isGreaterThan(expectedGreaterThan);
    }

    @When("User calls all cats facts endpoint")
    public void userCallsAllCatsFactsEndpoint() {
        catsApi = new CatsApi(FACTS);
    }
}