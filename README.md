### Cucumber Starter

This is a Java project that implements an API tests. The project uses Cucumber as a BDD framework and TestNG as a testing framework.

Note: I had no experience with GitLab CI/CD and cucumber, more time and work with the right technology is needed to better understand the best solutions for each case
and the API for testing has been changed to a more convenient one

### Changes Made:
1. Unused/extra files have been deleted.
2. Gradle has been removed and Maven is now being used exclusively.
3. The pom.xml file has been updated to reflect the changes made to the project.
4. The Steps class has been refactored to make it more concise and easier to understand.
5. The Api class has been updated to use a free API for testing purposes.
6. Files have been moved to more appropriate folders for better organization.
7. An Allure report has been added to provide a more convenient way to view test results.

### Prerequisites
Java 8 or later
Maven 3.0 or later
Git

### How to run the tests
The tests can be run by executing the TestRunner class or you can type in console 'mvn test'. You can specify the feature files to run in the pom.xml file.


### CI/CD with GitLab
The project includes a .gitlab-ci.yml file for configuring CI/CD in GitLab. The pipeline will run the tests automatically when changes are pushed to the repository.
You can also run job manually. You can open allure report in the deployment steps by opening Job artifacts and open index.html

### Project Structure
The project consists of the following components:

1. .feature files: these contain the scenarios that will be tested. You can add new scenarios or create a new .feature files
2. CatFactsSteps class: this class implements the Cucumber steps. Add new steps here or add a new steps files in steps folder
3. CatsApi class: this class implements the connection to the catfact.ninja API and provides methods for making API calls.
4. pom.xml: this file contains the project's configuration, including the dependencies and plugins used.

### Conclusion
This project provides a simple example of how to implement API tests
By using Cucumber and TestNG, the tests are easily understandable by both technical and non-technical stakeholders.
The CI/CD integration with GitLab makes it easy to run the tests on a regular basis and ensure that the API is working as expected.